TARGET = apostolofShell
CC = gcc
CFLAGS = -Wall -O3 -I.
OBJ = apostolofShellMain.o apostolofShellFunctions.o
DEPS = apostolofShellFunctions.h

.PHONY: default all clean

default: $(TARGET)
all: default

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

.PRECIOUS: $(TARGET) $(OBJ)

$(TARGET): $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)

clean:
	$(RM) *.o *~